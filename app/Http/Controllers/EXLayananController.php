<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LayananController extends Controller
{ 
    public function index()  {
        $klien_layanan_nama = DB::select('select * from public.klien_layanan_getall()');
        $id= env ('ID_KLIEN');
        $data_klien = DB::select("select * from public.klien_getone('$id')");
        // $poliklinikList = [
        //     ['name' => 'POLIKLINIK GIGI DAN MULUT', 'color' => '#B50AD0'],
        //     ['name' => 'POLIKLINIK IBU DAN ANAK', 'color' => '#0AD095'],
        //     ['name' => 'POLIKLINIK UMUM', 'color' => '#EA2B2B'],
        //     ['name' => 'POLIKLINIK KEBIDANAN', 'color' => '#2B83EA'],
        //     ['name' => 'POLIKLINIK MATA', 'color' => '#F2A532'],
        // ];
        
        return view('publik/layanan', compact('klien_layanan_nama','id','data_klien' ));
    }

    public function insert($id_klien,$urutan_layanan,$nama_layanan) {
        // Kode untuk menambahkan record pada tabel klien_layanan
	        $klien_layanan_status = '{
	        "data":[
                {
                    "status : 0,
                    "message :"Record berhasil ditambahkan"
                },
                {
                    "status : -1,
                    "message :"Gagal"
                },
                ]}';

        return $klien_layanan_status;
    }

        public function getAll($id_klien) {
        // Kode untuk menampilkan seluruh data pada tabel klien_layanan
        $list_klien_layanan = '{
            "status":0,
		    "message": "Penambahan berhasil",
            "data":[
                {
                    "id_klien" : 1,
                    "urutan_layanan" : A-01,
                    "nama_layanan" : "POLIKLINIK UMUM",
                },
                {
                    "id_klien" : 2,
                    "urutan_layanan" : A-02,
                    "nama_layanan" : "POLIKLINIK UMUM",
                },
                ]}';

        return $list_klien_layanan;
    }

    public function update($id_klien_layanan,$data_klien_layanan) {
        // Kode untuk melakukan perubahan record pada tabel klien_layanan
        $klien_layanan_status = '{
        "data":[
            {
                "status : 0,
                "message :"Record berhasil diperbarui"
            },
            {
                "status : -1,
                "message :"Gagal"
            },
            ]}';

        return $klien_layanan_status;
    }
    
    public function delete($id_klien_layanan) {
        // Kode untuk menghapus record pada tabel klien_layanan
        $status_delete = '{
        "data":[
            {
                "status : 0,
                "message :"Record berhasil ditambahkan"
            },
            {
                "status : -1,
                "message :"Gagal"
            },
            ]}';

        return $status_delete;
    }

    public function getOne($id_klien_konfigurasi) {
        // Kode untuk menampilkan satu data dari tabel klien_konfigurasi berdasarkan id_klien_konfigurasi
	$klien_konfigurasi_status = '{
	"data":[
	{
		"id_klien_layanan" : 1,
	},
	{
		"id_klien_layanan" : 2,
	}
]}';

        return $klien_konfigurasi_status;
    }
}
