<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function color()
    {
        return  view('admin.klien_color');
    }
    
    public function konfigurasi()
    {
        return  view('admin.konfigurasi_klien');
    }

    public function layanan()
    {
        return  view('admin.layanan_admin');
    }

    public function tambah_pengguna()
    {
        return  view('admin.tambah_pengguna');
    }
}
