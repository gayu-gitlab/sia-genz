<?php

namespace App\Http\Controllers;

use App\Models\KlienLayanan;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\User;
use Illuminate\Support\Facades\Cache;

class LayananAdminController extends Controller

{
    public function index()
    {
        // $klien_layanan_nama = DB::select('select * from public.klien_layanan_getall()');
        // $id= env ('ID_KLIEN');
        // $data_klien = DB::select("select * from public.klien_getone('$id')");
        // $klien_layanan = DB::select('select * from public.klien_layanan_getone(1)');
        // return view('admin/layanan_admin', compact('klien_layanan_nama','id','data_klien')); 
        $id_klien = env('ID_KLIEN');
        // dd($id_klien);

        $petugas = DB::table('pengguna')
            ->where('id_klien', '=', $id_klien)->get();
        // dd($petugas);
        return view('admin.layanan_admin', compact('petugas'));
    }

    public function dataLayanan()
    {
        $id_klien = env('ID_KLIEN');
        $dataLayananAdmin = DB::table('klien_layanan')->where('id_klien', '=', $id_klien)->get();
        // dd($dataLayananAdmin);
        return DataTables::of($dataLayananAdmin)
            ->addColumn('delete', function ($row) {
                return route('delete-klien-layanan', $row->id_klien_layanan);
            })->make(true);
    }


    public function input_klien_layanan(Request $request)
    {
        $id = env('ID_KLIEN');
        // $data_klien = DB::select("select * from public.klien_getone('$id')");
        // $urutan_layanan = 6;
        // $klien_layanan_getone = collect(DB::select('select id_klien_layanan from public.klien_layanan_getone(5)'))->first();
        // $klien_layanan_nama = DB::table('klien_layanan')->get();
        //    $proses_input = DB::select("SELECT * FROM public.klien_layanan_insert('$id','$klien_layanan_getone','$urutan_layanan','$data_klien')"); 
        //     dd($antrian_getone->id_klien_layanan);

        $validatedData = $request->validate(
            [
                'kode_layanan' => ['required', 'unique:klien_layanan', 'max:50'],
                'nama_layanan' => ['required', 'max:50'],
            ],
            [
                'kode_layanan.required' => 'Kode layanan harus diisi.',
                'kode_layanan.unique' => 'Kode layanan sudah digunakan.',
                'kode_layanan.max' => 'Kode layanan tidak boleh lebih dari 50 karakter.',
                'nama_layanan.required' => 'Nama layanan harus diisi.',
                'nama_layanan.max' => 'Nama layanan tidak boleh lebih dari 50 karakter.',
            ]
        );

        // $proses_input = DB::select("select * from public.klien_layanan_insert( '$id', '$request->kode_layanan','$request->nama_layanan')");
        // $proses_input = DB::table('klien_layanan')->insert([
        //     'id_klien' => $id,
        //     'kode_layanan' => $request->kode_layanan,
        //     'nama_layanan' => $request->nama_layanan
        // ]);

        DB::beginTransaction();

        try {

            $data = [
                'id_klien' => $id,
                'kode_layanan' => $request->kode_layanan,
                'nama_layanan' => $request->nama_layanan,
                'id_pengguna' => $request->petugas,
            ];

            $proses_input = DB::table('klien_layanan')->insert($data);

            DB::commit();
            return response()->json([
                'status'    => 'success',
                'toast'     => 'Data berhasil disimpan'
            ]);
        } catch (\Exception $e) {
            // dd($e);
            dd($e->getMessage());
            DB::rollback(); // something went wrong
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Data gagal disimpan'
            ]);
        }
    }
    public function update_klien_layanan(Request $request, $id_klien_layanan)
    {
        $nama_layanan_input = $request->nama_layanan;
        $kode_layanan_input = $request->kode_layanan;

        // Memastikan $id_klien_layanan dan $nama_layanan_input tidak kosong
        if (!empty($id_klien_layanan) && !empty($nama_layanan_input)) {
            // Menggunakan metode DB::table untuk mencari data klien_layanan berdasarkan id_klien_layanan
            $klien_layanan = DB::table('klien_layanan')
                ->where('id_klien_layanan', $id_klien_layanan)
                ->first();

            if ($klien_layanan) {
                // Jika data ditemukan, kita melakukan update
                $update_klien_layanan = DB::table('klien_layanan')
                    ->where('id_klien_layanan', $id_klien_layanan)
                    ->update(['nama_layanan' => $nama_layanan_input, 'kode_layanan' => $kode_layanan_input]);

                if ($update_klien_layanan) {
                    return response([
                        'status' => 'success',
                        'toast' => 'Update berhasil'
                    ]);
                } else {
                    return response([
                        'status' => -1,
                        'toast' => 'Gagal update layanan'
                    ]);
                }
            } else {
                return response([
                    'status' => -1,
                    'toast' => 'Data klien_layanan tidak ditemukan'
                ]);
            }
        } else {
            return response([
                'status' => -1,
                'toast' => 'ID atau nama layanan kosong'
            ]);
        }
    }

    public function hapus($id_klien_layanan)
    {
        DB::beginTransaction();
        try {
            $delete = DB::table('klien_layanan')->where('id_klien_layanan', '=', $id_klien_layanan)->delete();

            DB::commit();
            Cache::flush();
            return response()->json([
                'status'    => 'success',
                'toast'     => 'Data berhasil dihapus'
            ]);
        } catch (\Exception $e) {
            dd($e);
            DB::rollback(); // something went wrong
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Data gagal dihapus'
            ]);
        }
    }
}
