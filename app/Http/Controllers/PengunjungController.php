<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PengunjungController extends Controller
{
    public function index()
    {
        $id = env('ID_KLIEN');
        $klien_layanan_nama = DB::select("SELECT * FROM klien_layanan WHERE id_klien = '$id' ORDER BY kode_layanan ASC");
        return view('pengunjung.index', compact('klien_layanan_nama'));
    }

    public function form_daftar(string $id_klien_layanan)
    {
        $layanan = DB::select("SELECT * FROM klien_layanan WHERE id_klien_layanan = '$id_klien_layanan'");
        return view('pengunjung.daftar', compact('layanan'));
    }

    public function daftar(Request $request, string $id_klien_layanan)
    {
        $status = "Belum Dilayani";
        $jenis_pemanggilan = "Pengumuman";
        $id_pengguna = 10;
        $get_date = date('d-m-y');
        $get_time = date('h:i:sa');
        $date_now = date('Y-m-d');

        //get id_klien_layanan
        $id_layanan = $id_klien_layanan;

        //get data layanan
        $data_layanan = collect(DB::select("SELECT kode_layanan FROM klien_layanan WHERE id_klien_layanan = '$id_layanan'"))->first();

        $id = env('ID_KLIEN');
        $get_day_now = date('d');

        //get nomor antrian otomatis
        $no_antrian = DB::table('antrian')->select('no_antrian')
            ->where('kode_layanan', $data_layanan->kode_layanan)
            ->where('tanggal', $date_now)
            ->max('no_antrian') + 1;

        //proses insert antrian
        $proses_input_antrian = DB::select("select * from public.antrian_insert('$id','$id_layanan', '$data_layanan->kode_layanan', '$no_antrian', '$request->nama','$request->nomor_telepon', '$date_now')");

        //get id_antrian terakhir
        $get_id_antrian = DB::table('antrian')->select('id_antrian')->max('id_antrian');

        //proses memasukkan data ke tabel proses_antrian
        $proses_antrian = DB::select("SELECT * FROM public.proses_antrian_insert('$id', '$get_id_antrian', '$id_pengguna', '$jenis_pemanggilan', '$status', '$get_date', '$get_time')");

        //get data antrian
        $get_data_antrian = DB::table('antrian')
            ->select('id_antrian', 'nama_pengunjung', 'no_telp_pengunjung', 'kode_layanan', 'no_antrian')
            ->where('no_antrian', $no_antrian)
            ->where('kode_layanan', $data_layanan->kode_layanan)
            ->where('tanggal', $date_now)
            ->first();


        return view('pengunjung.tiket_antrian', compact('get_data_antrian'));
    }

    public function antrian()
    {
        $data = DB::select("SELECT proses_antrian.id_proses_antrian, antrian.no_antrian, antrian.nama_pengunjung, antrian.kode_layanan, antrian.id_klien_layanan, proses_antrian.status FROM antrian JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian ORDER BY proses_antrian.updated_at DESC LIMIT 1");
        $date_now = date('Y-m-d');

        $data_antrian_now = DB::table('proses_antrian')
            ->join('antrian', 'proses_antrian.id_antrian', '=', 'antrian.id_antrian')
            ->select('kode_layanan', 'no_antrian', 'antrian.tanggal')
            ->where('status', 'Selesai')
            ->where('antrian.tanggal', $date_now)
            ->orderByDesc('proses_antrian.updated_at')
            ->limit(1)
            ->first();
        // dd($data_antrian_now);


        $data_1 = DB::table('klien_layanan')
            ->join('antrian', 'klien_layanan.id_klien_layanan', '=', 'antrian.id_klien_layanan')
            ->join('proses_antrian', 'antrian.id_antrian', '=', 'proses_antrian.id_antrian')
            ->select('proses_antrian.id_proses_antrian', 'antrian.no_antrian', 'antrian.nama_pengunjung', 'antrian.kode_layanan', 'klien_layanan.id_klien_layanan', 'proses_antrian.status', 'klien_layanan.nama_layanan', 'antrian.tanggal')
            ->where('status', 'Belum Dilayani')
            ->where('antrian.id_klien_layanan', 2)
            ->where('antrian.tanggal', $date_now)
            ->first();

        $data_2 = DB::table('klien_layanan')
            ->join('antrian', 'klien_layanan.id_klien_layanan', '=', 'antrian.id_klien_layanan')
            ->join('proses_antrian', 'antrian.id_antrian', '=', 'proses_antrian.id_antrian')
            ->select('proses_antrian.id_proses_antrian', 'antrian.no_antrian', 'antrian.nama_pengunjung', 'antrian.kode_layanan', 'klien_layanan.id_klien_layanan', 'proses_antrian.status', 'klien_layanan.nama_layanan', 'antrian.tanggal')
            ->where('status', 'Belum Dilayani')
            ->where('antrian.id_klien_layanan', 3)
            ->where('antrian.tanggal', $date_now)
            ->first();

        $data_3 = DB::table('klien_layanan')
            ->join('antrian', 'klien_layanan.id_klien_layanan', '=', 'antrian.id_klien_layanan')
            ->join('proses_antrian', 'antrian.id_antrian', '=', 'proses_antrian.id_antrian')
            ->select('proses_antrian.id_proses_antrian', 'antrian.no_antrian', 'antrian.nama_pengunjung', 'antrian.kode_layanan', 'klien_layanan.id_klien_layanan', 'proses_antrian.status', 'klien_layanan.nama_layanan', 'antrian.tanggal')
            ->where('status', 'Belum Dilayani')
            ->where('antrian.id_klien_layanan', 4)
            ->where('antrian.tanggal', $date_now)
            ->first();

        $data_4 = DB::table('klien_layanan')
            ->join('antrian', 'klien_layanan.id_klien_layanan', '=', 'antrian.id_klien_layanan')
            ->join('proses_antrian', 'antrian.id_antrian', '=', 'proses_antrian.id_antrian')
            ->select('proses_antrian.id_proses_antrian', 'antrian.no_antrian', 'antrian.nama_pengunjung', 'antrian.kode_layanan', 'klien_layanan.id_klien_layanan', 'proses_antrian.status', 'klien_layanan.nama_layanan', 'antrian.tanggal')
            ->where('status', 'Belum Dilayani')
            ->where('antrian.id_klien_layanan', 5)
            ->where('antrian.tanggal', $date_now)
            ->first();

        $data_5 = DB::table('klien_layanan')
            ->join('antrian', 'klien_layanan.id_klien_layanan', '=', 'antrian.id_klien_layanan')
            ->join('proses_antrian', 'antrian.id_antrian', '=', 'proses_antrian.id_antrian')
            ->select('proses_antrian.id_proses_antrian', 'antrian.no_antrian', 'antrian.nama_pengunjung', 'antrian.kode_layanan', 'klien_layanan.id_klien_layanan', 'proses_antrian.status', 'klien_layanan.nama_layanan', 'antrian.tanggal')
            ->where('status', 'Belum Dilayani')
            ->where('antrian.id_klien_layanan', 6)
            ->where('antrian.tanggal', $date_now)
            ->first();



        $data_nama_layanan = collect(DB::table('klien_layanan')
            ->select('nama_layanan')
            ->orderBy('kode_layanan')
            ->get());

        // dd($data_nama_layanan);

        // $data1 = DB::select("SELECT proses_antrian.id_proses_antrian, antrian.no_antrian, antrian.nama_pengunjung, antrian.kode_layanan, klien_layanan.id_klien_layanan, proses_antrian.status, klien_layanan.nama_layanan FROM klien_layanan JOIN antrian ON klien_layanan.id_klien_layanan = antrian.id_klien_layanan JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian WHERE status = 'Belum Dilayani' and klien_layanan.id_klien_layanan = '2' ORDER BY no_antrian ASC LIMIT 1");
        // $data2 = DB::select("SELECT proses_antrian.id_proses_antrian, antrian.no_antrian, antrian.nama_pengunjung, antrian.kode_layanan, klien_layanan.id_klien_layanan, proses_antrian.status, klien_layanan.nama_layanan FROM klien_layanan JOIN antrian ON klien_layanan.id_klien_layanan = antrian.id_klien_layanan JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian WHERE status = 'Belum Dilayani' and klien_layanan.id_klien_layanan = '3' ORDER BY no_antrian ASC LIMIT 1");
        // $data3 = DB::select("SELECT proses_antrian.id_proses_antrian, antrian.no_antrian, antrian.nama_pengunjung, antrian.kode_layanan, klien_layanan.id_klien_layanan, proses_antrian.status, klien_layanan.nama_layanan FROM klien_layanan JOIN antrian ON klien_layanan.id_klien_layanan = antrian.id_klien_layanan JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian WHERE status = 'Belum Dilayani' and klien_layanan.id_klien_layanan = '4' ORDER BY no_antrian ASC LIMIT 1");
        // $data4 = DB::select("SELECT proses_antrian.id_proses_antrian, antrian.no_antrian, antrian.nama_pengunjung, antrian.kode_layanan, klien_layanan.id_klien_layanan, proses_antrian.status, klien_layanan.nama_layanan FROM klien_layanan JOIN antrian ON klien_layanan.id_klien_layanan = antrian.id_klien_layanan JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian WHERE status = 'Belum Dilayani' and klien_layanan.id_klien_layanan = '5' ORDER BY no_antrian ASC LIMIT 1");
        // $data5 = DB::select("SELECT proses_antrian.id_proses_antrian, antrian.no_antrian, antrian.nama_pengunjung, antrian.kode_layanan, klien_layanan.id_klien_layanan, proses_antrian.status, klien_layanan.nama_layanan FROM klien_layanan JOIN antrian ON klien_layanan.id_klien_layanan = antrian.id_klien_layanan JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian WHERE status = 'Belum Dilayani' and klien_layanan.id_klien_layanan = '6' ORDER BY no_antrian ASC LIMIT 1");

        return view('pengunjung.antrian', compact('data', 'data_antrian_now', 'data_nama_layanan', 'data_1', 'data_2', 'data_3', 'data_4', 'data_5'));
    }

    public function cetak_pdf(string $id_antrian)
    {
        $get_data_antrian = DB::table('antrian')
            ->join('klien_layanan', 'antrian.id_klien_layanan', '=', 'klien_layanan.id_klien_layanan')
            ->select('id_antrian', 'antrian.kode_layanan', 'no_antrian', 'nama_pengunjung', 'klien_layanan.nama_layanan')
            ->where('id_antrian', $id_antrian)->first();

        $date_now = date('d-m-Y');
        $time_now = date('h:i:s');
        return view('pengunjung.antrian_pdf', compact('get_data_antrian', 'date_now', 'time_now'));
    }
}
