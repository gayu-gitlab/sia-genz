<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthLoginController extends Controller
{
    public function login()
    {
        return view('/login');
    }

    public function login_pengguna()
    {
        return view('/pengguna/login_pengguna');
    }

    public function lupa_password()
    {
        return view('/lupa_password');
    }

    public function proseslogin(Request $request)
    {
        $user = DB::table('klien')->where('email', '=', $request->email)->where('password', '=', $request->password)->first();

        if ($user != null) {
            return redirect()->intended('/admin/index');
        } else {
            return back()->withErrors(['email' => 'Email atau password salah.'])->withInput();
        }
    }

    public function proseslogin_pengguna(Request $request)
    {
        $user = DB::table('pengguna')->where('email', '=', $request->email)->where('password_pengguna', '=', $request->password_pengguna)->first();

        if ($user != null) {
            return redirect()->intended('petugas/pemanggilan');
        } else {
            return back()->withErrors(['email' => 'Email atau password salah.'])->withInput();
        }
    }

   
        public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
