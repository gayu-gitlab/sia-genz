<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Antrian;
use Carbon\Carbon;

class AntrianController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // $id = env('ID_KLIEN');
        // $data_klien = DB::select("select * from public.klien_getone('$id')");
        // $antrian_getone = DB::select('select * from public.klien_layanan_getone(5)'); 
        // return view('publik/antrian', compact('data_klien', 'antrian_getone'));
    }

    public function insert(Request $request)
    {
        //Insert untuk pengambilan antrian pada tabel antrian
        $id = env('ID_KLIEN');
        $antrian_getone = collect(DB::select('select id_klien_layanan from public.klien_layanan_getone(5)'))->first();
        // dd($antrian_getone->id_klien_layanan);

        $kode_layanan = $this->getKodeLayanan($request->$antrian_getone);
        $no_antrian = $this->getNoAntrian($kode_layanan);

        DB::select("select * from public.antrian_insert('$id', '$antrian_getone->id_klien_layanan', '$kode_layanan', '$no_antrian', '$request->nama', '$request->nomor_telepon'')");

        // DB::select("select * from public.antrian_insert('$id', '$antrian_getone->id_klien_layanan', '$request->nama', '$request->nomor_telepon','$no_antrian')");
        //  return $antrian_getone;


        // $status_antrian_json = '{ 
        //     "data": [
        //     {
        //         "status" : "0",
        //         "message" : "Berhasil ditambahkan!"
        //     },
        //     {
        //         "status" : "-1",
        //         "message" : "Gagal ditambahkan"
        //     }
        //     ]}';
        // return $status_antrian_json;
    }

    public function update($id_klien, $id_antrian, $id_klien_layanan, $arr_antrian)
    {
        //Update untuk mengupdate antrian pada tabel antrian
        $status_antrian_json = '{
            "data": [
            {
                "status" : "0",
                "message" : "Berhasil diupdate!"
            },
            {
                "status" : "-1",
                "message" : "Gagal diupdate!"
            }
            ]}';
        return $status_antrian_json;
    }

    public function delete()
    {

        // DB::select("select * from public.antrian_delete('2')");
        // $delete = collect(DB::select('select id_antrian from public.antrian_delete'))->first(); 
        // dd($antrian_getone->id_klien_layanan);
        //Delete untuk menghapus antrian pada tabel antrian
        $status_antrian_json = '{
            "data": [
            {
                "status" : "0",
                "message" : "Berhasil dihapus!"
            },
            {
                "status" : "-1",
                "message" : "Gagal dihapus!"
            }
            ]}';
        // return $status_antrian_json;
    }


    public function getAll($id_antrian, $id_klien)
    {
        //getAll untuk mengambil data pada tabel antrian

        $list_antrian_json = '{
            "status" : "0",
            "message" : "Berhasil ditambahkan",
            "data": 
            {
                "id_antrian" 		: "1",
                "id_klien" 			: "K1",
                "id_klien_layanan"	: "KL1",
                "id_pengguna"		: "P1",
                "nama_pengunjung"		: "Georgia",
                "no_telp_pengunjung"	: "085285285285",
                "no_antrian"		: "A-10",
                "created_at"		: "2023-08-23 11:10:30"
            },
            {
                "id_antrian" 		: "2",
                "id_klien" 			: "K2",
                "id_klien_layanan"	: "KL2",
                "id_pengguna"		: "P2",
                "nama_pengunjung"		: "Georgia",
                "no_telp_pengunjung"	: "085285285213",
                "no_antrian"		: "A-11",
                "created_at"		: "2023-08-23 11:22:30" 
            }
        }';
        return $list_antrian_json;
    }

    private function getKodeLayanan($antrian_getone)
    {
        $query = DB::table('klien_layanan')->select('kode_layanan')->where('id_klien_layanan', '=', $antrian_getone->id_klien_layanan)->first();

        $kode_layanan = $query->first()->kode_layanan;
        return $kode_layanan;
    }

    // private function getNoAntrian($kode_layanan)
    // {
    //     $no_antrian = DB::table('antrian')->select('no_antrian')->where('kode_layanan', $kode_layanan)->max('no_antrian') + 1;
    //     return $no_antrian;
    // }
    private function getNoAntrian($kode_layanan)
    {
        // Ambil tanggal sekarang
        $tanggal_sekarang = Carbon::now()->format('Y-m-d');

        // Cari data no_antrian dengan kode_layanan dan tanggal yang sama
        $no_antrian_db = DB::table('antrian')->where('kode_layanan', $kode_layanan)->where('created_at', '=', Carbon::now()->format('Y-m-d'))->first();

        // Jika data ditemukan
        if ($no_antrian_db) {
            $no_antrian = $no_antrian_db->no_antrian;
            $no_antrian++;

            // Update no_antrian di database
            $no_antrian_db->update(['no_antrian' => $no_antrian]);
        } else {
            // Jika data tidak ditemukan, berarti hari/tanggal telah berganti

            // Ambil tanggal kemarin
            $tanggal_kemarin = Carbon::now()->subDay(1)->format('Y-m-d');

            // Jika tanggal telah berganti
            if ($tanggal_sekarang != $tanggal_kemarin) {
                // Set no_antrian menjadi 0
                $no_antrian = 0;
            } else {
                // Set no_antrian menjadi max no_antrian + 1
                $no_antrian = DB::table('antrian')->where('kode_layanan', $kode_layanan)->max('no_antrian') + 1;
            }
        }  
    }
}
