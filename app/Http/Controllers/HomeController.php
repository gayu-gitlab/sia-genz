<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function profil(){
        $id = 1;

        $data_pengguna = DB::select("select * from public.pengguna_getone('$id')");
        return view ('profil', compact('data_pengguna'));
    }
}
