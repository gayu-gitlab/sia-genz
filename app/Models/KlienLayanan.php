<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KlienLayanan extends Model
{
    use HasFactory;

    protected $table = 'klien_layanan';
    protected $primaryKey = 'id_klien_layanan';
    protected $fillable = ['nama_layanan'];
}
