@extends ('pengunjung/layout/template')

@section('content')
    <div class="container">
        <h1><strong>PILIH LAYANAN</strong></h1>
        <div class="row g-2 mt-4">
            @foreach ($klien_layanan_nama as $index => $poli)
                <div class="d-grid col-4 mx-auto">
                    <a class="button-klien-layanan poli-{{ $index + 1 }}"
                        href="pengunjung/daftar/{{ $poli->id_klien_layanan }}"
                        style="text-decoration: none; padding: 30px 0; font-size: 16pt;">{{ $poli->nama_layanan }}</a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
