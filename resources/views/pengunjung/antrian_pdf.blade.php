<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/klinikya.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <title>Tiket Antrian</title>
</head>

<body>
    <div class="mt-5 d-flex justify-content-center row">
        <div class="tiket-antrian col-6 py-3">
            <div class="header-tiket text-center">
                <?php
                $id_klien = env('ID_KLIEN');
                $header_klien = DB::select("SELECT * FROM public.klien_getone('$id_klien')");
                ?>
                @foreach ($header_klien as $data)
                    <h4>{{ $data->header_klien }}</h4>
                @endforeach
                <p>{{ $get_data_antrian->nama_layanan }}</p>
            </div>
            <hr>
            <p>NOMOR:</p>
            <div class="nomor-antrian text-center py-3">
                <h1 style="font-size: 64px; font-weight:bold;">
                    {{ "$get_data_antrian->kode_layanan - $get_data_antrian->no_antrian" }}</h1>
            </div>
            <hr>
            <div class="footer-antrian text-center">
                <p style="color: #6d6d6d"><i class="fa-solid fa-calendar px-1"></i>{{ "$date_now" }}<i
                        class="fa-solid fa-clock px-1 ms-3"></i>
                    {{ $time_now }}</p>
            </div>
        </div>
    </div>
    <script>
        window.print();
        setTimeout(function() {
            window.location.href = '/pengunjung';
        }, 2000);
    </script>
</body>

</html>

@push('scripts')
@endpush
