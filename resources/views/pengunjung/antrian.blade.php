@extends ('pengunjung/layout/template')
@section('content')
    <div class="container">
        <div class="antrian-saat-ini row">
            <div class="p-2 col-4">
                <div class="card text-dark bg-light">
                    <div class="card-header text-center">{{ $data_nama_layanan['0']->nama_layanan }}</div>
                    <div class="card-body">
                        <?php
                        if ($data_1 == null) {
                            echo "<h1 class='card-title align-middle'> - </h1>";
                        } else {
                            echo "<h1 class='card-title align-middle'>$data_1->kode_layanan - $data_1->no_antrian</h1>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="p-2 col-4">
                <div class="card text-dark bg-light">
                    <div class="card-header text-center">{{ $data_nama_layanan['1']->nama_layanan }}</div>
                    <div class="card-body">
                        <?php
                        if ($data_2 == null) {
                            echo "<h1 class='card-title align-middle'> - </h1>";
                        } else {
                            echo "<h1 class='card-title align-middle'>$data_2->kode_layanan - $data_2->no_antrian</h1>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="p-2 col-4">
                <div class="card text-dark bg-light">
                    <div class="card-header text-center">{{ $data_nama_layanan['2']->nama_layanan }}</div>
                    <div class="card-body">
                        <?php
                        if ($data_3 == null) {
                            echo "<h1 class='card-title align-middle'> - </h1>";
                        } else {
                            echo "<h1 class='card-title align-middle'>$data_3->kode_layanan - $data_3->no_antrian</h1>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="p-2 col-4">
                <div class="card text-dark bg-light">
                    <div class="card-header text-center">{{ $data_nama_layanan['3']->nama_layanan }}</div>
                    <div class="card-body">
                        <?php
                        if ($data_4 == null) {
                            echo "<h1 class='card-title align-middle'> - </h1>";
                        } else {
                            echo "<h1 class='card-title align-middle'>$data_4->kode_layanan - $data_4->no_antrian</h1>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="p-2 col-4">
                <div class="card text-dark bg-light">
                    <div class="card-header text-center">{{ $data_nama_layanan['4']->nama_layanan }}</div>
                    <div class="card-body">
                        <?php
                        if ($data_5 == null) {
                            echo "<h1 class='card-title align-middle'> - </h1>";
                        } else {
                            echo "<h1 class='card-title align-middle'>$data_5->kode_layanan - $data_5->no_antrian</h1>";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
