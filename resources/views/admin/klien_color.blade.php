@extends('admin/layout/dashboard')

@section('page-title')
    <h3>Klien Color</h3>
@endsection

@section('section')
    <div class="klien-color row">
        @foreach ($data_klien_color_getone as $data)
            <div class="col-6">
                <div class="update-klien-color p-4">
                    <h4 class="mb-4">UPDATE WARNA WEBSITE</h4>
                    <form>
                        <div class="mb-3">
                            <label class="form-label">Header</label>
                            <div class="row">
                                <div class="col-10">
                                    <input id="value" type="text" class="form-klien-color"
                                        value="{{ $data->header_color }}" required>
                                </div>
                                <div class="col-2">
                                    <input id="color-picker" class="input-color" type="color" name="header_color"
                                        value="{{ $data->header_color }}">
                                    {{-- <div class="input-color" ></div> --}}
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Footer</label>
                            <div class="row">
                                <div class="col-10">
                                    <input type="text" class="form-klien-color" value="{{ $data->footer_color }}"
                                        required>
                                </div>
                                <div class="col-2">
                                    <input class="input-color" type="color" name="header_color"
                                        value="{{ $data->footer_color }}">
                                    {{-- <div class="input-color" style="background-color: {{ $data->footer_color }};"></div> --}}
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Text</label>
                            <div class="row">
                                <div class="col-10">
                                    <input type="text" class="form-klien-color" value="{{ $data->text_color }}" required>
                                </div>
                                <div class="col-2">
                                    <input class="input-color" type="color" name="header_color"
                                        value="{{ $data->text_color }}">
                                    {{-- <div class="input-color" style="background-color: {{ $data->text_color }};"></div> --}}
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Button Text</label>
                            <div class="row">
                                <div class="col-10">
                                    <input type="text" class="form-klien-color" value="{{ $data->button_text_color }}"
                                        required>
                                </div>
                                <div class="col-2">
                                    <input class="input-color" type="color" name="header_color"
                                        value="{{ $data->button_text_color }}">
                                    {{-- <div class="input-color" style="background-color: {{ $data->button_text_color }};"></div> --}}
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Button</label>
                            <div class="row">
                                <div class="col-10">
                                    <input type="text" class="form-klien-color" value="{{ $data->button_color }}"
                                        required>
                                </div>
                                <div class="col-2">
                                    <input class="input-color" type="color" name="header_color"
                                        value="{{ $data->button_color }}">
                                    {{-- <div class="input-color" style="background-color: {{ $data->button_color }};"></div> --}}
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success" style="width: 100%;">Simpan</button>
                    </form>

                </div>
            </div>
            <div class="col-6">
                <div class="overview-klien-color p-4">
                    <h4 class="mb-4">OVERVIEW</h4>
                    <div class="card-mockup">
                        <div class="header">
                            <h2 class="text-center" style="padding: 10px 0 0 0; color: white;">header</h2>
                        </div>
                        <div class="main">
                            <h2 id="color-picker" class="text-center teks">text</h2>
                            <form action="/login" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="form text" />
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="form text" />
                                </div>
                                <input placeholder="button text" class="btn"
                                    style=" background-color: {{ $data->button_color }}; color: {{ $data->button_text_color }}" />
                            </form>
                        </div>
                        <div class="footer-preview">
                            <p class="text-center"
                                style="padding: 10px 0 0 0; background-color: {{ $data->footer_color }};">footer
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <style>
            .card-mockup {
                width: 330px;
                height: 622px;
                margin: 0 auto;
                background-color: #fff;
                border-radius: 15px;
                display: flex;
                flex-direction: column;
                border: 1px solid black;
            }

            .header {
                width: 100%;
                height: 60px;
                background-color: #000;
                border-radius: 15px 15px 0 0;
                color: #fff;
                text-align: center;
                font-size: 20px;
            }

            .teks {
                margin: 93px 0;
            }

            .main {
                width: 100%;
                height: 500px;
                background-color: #fff;
            }

            .form-group {
                margin-bottom: 39px;
            }

            .form-control {
                width: 85%;
                height: 47px;
                border-radius: 5px;
                border: 1px solid #ccc;
                margin: 0 auto;
                display: flex;
                align-items: center;
            }

            .btn {
                width: 85%;
                height: 47px;
                border-radius: 5px;
                margin: 0 auto;
                display: flex;
                align-items: center;
            }

            .footer-preview {
                width: 100%;
                height: 60px;
                color: #fff;
                text-align: center;
                font-size: 20px;
                border-radius: 0 0 15px 15px;
            }

            #color-picker::-webkit-color-swatch {
                border-radius: 10%;
            }
        </style>
        <script>
            var colorPicker = document.getElementById("color-picker");
            var colorValue = document.getElementById("value");

            colorValue.addEventListener("input", function() {
                colorPicker.value = colorValue.value;
            });
        </script>
    @endsection
