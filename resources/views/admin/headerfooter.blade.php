@extends ('admin/layout/dashboard')
@section('section')

<div class="content-dashboard row">
        <div class="mx-4 my-2 p-4 col-6"
            style="background-color: white; border-radius: 5px; box-shadow: 5px 5px 35px #00000022; margin-left: 20px;">

<form class="xform" action="/update-header-footer" method="post">
    @csrf
    <div class="mb-3">
        <label for="header_klien" class="form-label">Teks Header</label>
        <input type="text" name="header_klien" value="{{ $data_header_footer->header_klien }}" class="form-control"
         style="background-color:rgb(244, 244, 244)">
    </div>
    <div class="mb-3">
        <label for="footer_klien" class="form-label">Teks Footer</label>
        <input type="text" name="footer_klien" value="{{ $data_header_footer->footer_klien }}" class="form-control"
         style="background-color:rgb(244, 244, 244)">
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>
</div>
    <!-- <div class="content-dashboard row">
        <div class="mx-4 my-2 p-4 col-6"
            style="background-color: white; border-radius: 5px; box-shadow: 5px 5px 35px #00000022; margin-left: 20px;">
            <form>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Teks Header</label>
                    <input type="text" value="{{ $data_header_footer->header_klien }}" class="form-control"
                        style="background-color:rgb(244, 244, 244)">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Teks Footer</label>
                    <input type="text" value="{{ $data_header_footer->footer_klien }}" class="form-control"
                        style="background-color:rgb(244, 244, 244)">
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div> -->
@endsection
