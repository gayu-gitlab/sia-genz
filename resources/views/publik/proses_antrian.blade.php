@extends ('publik/layout/template')
@section('content')
    <div class="content">
        <div class="text-center grid d-grid gap-3">
            <h1>Nomor antrian Anda</h1>
            <h1 class="no_antrian fw-bold">{{ "$get_data_antrian1->kode_layanan - $get_data_antrian1->no_antrian" }}</h1>
            <h1>Nomor antrian saat ini</h1>
            <h1 class="no_antrian fw-bold">{{ "$antrian_saat_ini->kode_layanan - $antrian_saat_ini->no_antrian" }}</h1>
            <h3>Yang terhormat Bapak/Ibu:</h3>
            <h3 class="fw-bold">{{ $get_data_antrian1->nama_pengunjung }}</h3>
            <p>Giliran Anda akan segera tiba. Mohon untuk tetap berada di dalam ruangan. Terima Kasih.</p>
        </div>
    </div>
@endsection
