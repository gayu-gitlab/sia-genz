<!DOCTYPE html>
<html lang="en">

<head>
    {{-- <meta http-equiv="refresh" content="5"> --}}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/klinikya.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/vendor/toastr/build/toastr.min.css') }}">
    <title>Publik</title>
</head>

<body>
    <!-- header -->
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #1964D5;">
        <div class="container-fluid d-flex justify-content-center judul">

            <?php
            $id_klien = env('ID_KLIEN');
            $header_klien = DB::select("SELECT * FROM public.klien_getone('$id_klien')");
            
            ?>
            @foreach ($header_klien as $data)
                <strong>{{ $data->header_klien }}</strong>
            @endforeach
        </div>
    </nav>

    <!-- waktu -->
    <div class="container-fluid waktu mt-2">
        <div class="date-container">
            <div id="current-date">
                {{ now()->format('d F Y') }}
            </div>
            <div id="current-time">
                {{ now()->format('h:m:s') }}
            </div>
        </div>
    </div>

    <!-- isi rating -->
    <div class="container-fluid d-flex justify-content-center">
        @yield('content')
    </div>

    <!-- footer -->
    <div class="container-fluid footer pt-4 pb-2">
        <div class="d-flex justify-content-center">
            <?php
            $id_klien = env('ID_KLIEN');
            $footer_klien = DB::select("SELECT * FROM public.klien_getone('$id_klien')");
            
            ?>
            @foreach ($footer_klien as $data)
                <p>{{ $data->footer_klien }}</p>
            @endforeach
        </div>
    </div>
    <script src="{{ asset('assets/js/myfunction.js') }}"></script>
    <script src="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/toastr/build/toastr.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/pace-progress/pace.min.js') }}"></script>
</body>
@stack('script')
<script>
    function updateTime() {
        var now = new Date();
        var formattedTime = now.toLocaleTimeString();

        document.getElementById('current-time').textContent = formattedTime;

        setTimeout(updateTime, 1000); // Update time every second
    }

    updateTime(); // Initial call
</script>

</html>
