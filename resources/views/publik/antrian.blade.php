@extends('publik/layout/template')

@section('content')
    <div class="content">
        <a href="/" class="btn btn-secondary"><i class="fa-solid fa-angle-left pe-2"></i>Kembali</a>
        <h1><strong> Form Antrian </strong></h1><br />
        @foreach ($layanan as $data)
            <h1><strong>{{ $data->nama_layanan }}</strong></h1>

            <form action="/layanan/input/store/{{ $data->id_klien_layanan }}" method="post">
                
                @csrf
                <div class="mb-3 mt-4  ">
                    <label for ="exampleFormControlInput1" class="form-label">Nama (Optional)</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name="nama"
                        placeholder="Nama">
                </div>
                <label for="exampleFormControlInput2" class="form-label">Nomor Telepon</label>
                <input type="text" class="form-control" id="exampleFormControlInput2" name="nomor_telepon"
                    placeholder="Nomor Telepon" value="62" required>
                <p class="form-text text ">
                    Nomor telepon dapat disamakan dengan nomor WA untuk memperoleh salinan tiket setelah memperoleh tiket
                    antrian.
                </p>

                <div class="d-grid gap-2 mt-4">
                    <button class="btn btn-lg" type="submit" style="background-color: #1964D5; color:white;">Kirim</button>
                </div>
            </form>
        @endforeach
    </div>
@endsection
