<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Petugas</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/klinikya.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/dashboard/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</head>

<body>
    <div class="wrapper d-flex align-items-stretch">
        <nav id="sidebar">
            <div class="p-4 menu-samping">
                <h4><a href="index.html" class="logo text-white">Petugas</a></h4>
                <hr style="border: 1px solid white;">
                <ul class="list-unstyled components">
                    <li class="{{ request()->is('petugas/pemanggilan') ? 'active-menu active' : '' }}">
                        <a href="/petugas/pemanggilan"><i class="fa-solid fa-list-ol link-sidebar"></i>Pemanggilan</a>
                    </li>
                    <li class="{{ request()->is('petugas/antrian_pengguna') ? 'active-menu active' : '' }}">
                        <a href="/petugas/antrian_pengguna"><i
                                class="fa-solid fa-clipboard-list link-sidebar"></i>Antrian</a>
                    </li>
                    <li class="{{ request()->is('petugas/feedback') ? 'active-menu active' : '' }}">
                        <a href="/petugas/feedback"><i class="fa-solid fa-comments link-sidebar"></i>Feedback</a>
                    </li>
                    <li class="{{ request()->is('profile') ? 'active-menu active' : '' }}">
                        <a href="/profile"><i class="fa-solid fa-user link-sidebar"></i>Profile</a>
                    </li>

                </ul>
                <div class="bottom-menu">
                    <ul class="list-unstyled components">
                        <li class="">
                             <!-- Tombol Logout -->
                             <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                            <a href="#" onclick="confirmLogout()"><i class="fa-solid fa-right-from-bracket link-sidebar"></i>Logout</a>

                            <script>
                                function confirmLogout() {
                                    if (confirm("Apakah Anda yakin ingin keluar?")) {
                                        event.preventDefault();
                                        document.getElementById('logout-form').submit();
                                    }
                                }
                            </script>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="content">
            <nav class="navbar navbar-light" style="background-color: #fff;">
                <div class="container-fluid">
                    <div>
                        <h3>SIA (Sistem Informasi Antrian)</h3>
                    </div>
                    <div class="d-flex">
                        <div class="dropdown">
                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <img class="profile" src="{{ asset('assets/image/person1.jpg') }}">
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="#">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="p-3">
                @yield('section')
            </div>
        </div>
    </div>
    @stack('scripts')
</body>

</html>
