@extends ('pengguna/layout/dashboard')
@section('section')
    <div class="section container-fluid">
        <h4 class="my-4">Konfigurasi</h4>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
                Konfigurasi 1
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
                Konfigurasi 2
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
            <label class="form-check-label" for="flexCheckChecked">
                Konfigurasi 3
            </label>
        </div>
        <input class="btn btn-primary btn-sm" type="submit" value="Simpan">
    </div>
@endsection
